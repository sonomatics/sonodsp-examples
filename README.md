# SonoDSP Examples

This project has two files: One contains some SonoDSP examples in C++. The other is a prototype of a scripting language demonstrating scripts identical to the corresponding C++ examples.

SonoDSP was developed out of years of frustration with Csound and other text-based audio-dsp environments.  It seemed that many were in an awkward middle-ground between a robust programming language and basic scripting. SonoDSP is an attempt to create a C++ framework that could be simpler to use than Csound, but allowing the user to dive deeper, when needed.  

However, there are times it would be useful to be able to specify a patch without having to deal with C++ syntax. A simple scripting language could both be used for real-time (including live-coding) and as a pre-processing pass to compile into an app. 

The scripting language should never violate the original principles of the SonoDSP project, listed below.


**SonoDSP Principles**

* Use a general purpose programming language
	- lots of things you “get for free”
	- no need to learn another language
	- easy to incorporate other code into your own projects
	- teaching incentive: learning this language could gain experience for other opportunities
	- potentially platform-independent 
    -

* Use a general purpose IDE
	- code editor, syntax highlighting, documentation quickview, auto completion
	- a debugger!
	- can create native apps and plugins
	- robust GUI design tools
    -

* Default to the highest quality (which can always be reduced for optimization)
 	- ugens that are able to run at audio rate should do so by default
	- cubic interpolation 
	- sinc sr conversion
    -

* Have as many parameters as possible default to reasonable values
	- so everything “just works” by default
    -

* Keep the “core library” of ugens as small as possible
	- no bloatware
	- only add new ugens to the core that are absolutely necessary
	- 3rd-party ugens will always be "opt-in" via a package manager or separate targets or something 
    - backwards compatibility is lower priority, but still kept by moving old elements to the package manager

* Make it as easy as possible to define your own ugens
	- in fact, “using” the library can be just a process of defining your own ugens and instantiating them.
    -

* Real-time and File rendering should be identical

**Architecture**
* Object Oriented
	- Everything is derived from an "Ugen" base class:
		- Ugens
		- Functions (a Ugen with one input and one output)
		- Patch (a collection of Ugens)
		- Instrument (a Patch with a start time and duration)
		- Session (a “master” Patch, that handles:)
			+ audio i/o to/from the operating system (live or to file)
			+ the master list of all defined instruments 
			+ scheduling of instruments
			+ global data (sr, etc) accessible by all Ugens
			+ "always on" Ugens and Patches (aka "Instrument 0" in Csound)
		- Connection ( a struct representing a “signal” [buffer], 
	-

* “Graph” paradigm, with Signals, Inlets, and Outlets
