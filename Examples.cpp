//
//  main.cpp
//  SonoDSP
//
//  Created by matt on 5/30/20.
//  Copyright © 2020 sonomatics. All rights reserved.
//

#include <iostream>

#include "sonodsp.hpp"

#pragma mark - HELLO WORLD!

void HelloWorld()
{
    // create our session (stereo)
    MacOSStereoSession ourSession;
    
    // create our oscillator with default parameters
    auto asine = new Sine();
    
    // add it to our session "patch"
    ourSession.AddUgen(asine);
    
    // hook it to the outputs
    ourSession.MakeConnectionToPatchOutlets(asine, asine);
    
    std::cout << "Hello, World!" << std::endl;
    
    // run continuously until process is killed
    ourSession.Start();
}



#pragma mark - HELLO WORLD, MORE DETALS!

void HelloWorldMore()
{
    // Sessions function as our "top-level" struct, handling:
    // + audio i/o to/from the operating system.
    // + event scheduling
    // + global data (sr, etc) accessible by all unit generators
    //
    // Sessions are fully functioning patches, so you can
    // treat the session as a kind of "global" patch and add
    // unit generators to them, direclty (as we do below)
    //
    // For MacOS, you can currently use MacOSStereoSession,
    // MacOSStereoIOSession (for input), or FileSession (to render to a file)
    // Here, we use MacOSStereoSession (stereo out only) with a sr of 96000
    //
    // A cross-platform session is planned using JUCE (macOS, win, linux, AU, VST, iOS, android, etc)

    MacOSStereoSession ourSession(96000);
    
    
    // Ugens make up the major part of the library.
    // Each Ugen (aka "unit generator") inherit from the "Ugen" base class.
    // Ugens have input parameters at initialization (most have optional defaults)
    // and have inlets for dynamic control.
    // [ option click on constructor to see what the inlet parameters, defaults, and inlets are! ]
    
    auto modulator = new Sine(3000, .5);   // amp = 3000, cps = .5
    auto carrier = new Sine(.5);           // amp = .5
    
    
    // Most ugens have "setters" and "getters" for properties.
    // So you could use setters to initialize a ugen too (at least for common parameters)
    Sine *oscil = new Sine();
    oscil->SetAmp(.5);
    oscil->SetCps(440);
    oscil->SetLimit(true);
    oscil->SetPhase(.25);  // now a cosine
    ourSession.AddUgen(oscil);

    
    // add the modulator to the session
    ourSession.AddUgen(modulator);
    
    // "Add Ugen" has optional input parameters to pass other unit generators to automatically connect it
    // to an inlet of the unit generator being added. This is optional - another way to connect
    // unit generators is to use the MakeConnection() function instead.
    //
    // Here, as we add the carrier oscillator, we connect
    // the modulator's outlet to the carrier's 2nd inlet (frequency)
    
    ourSession.AddUgen(carrier, nullptr, modulator);
    
    // hook output of the carrier to both speakers
    ourSession.MakeConnectionToPatchOutlets(carrier, carrier);
    
    // to generate sound, either call Run() [which will block until finished] or Start()
    ourSession.Run(10);
    
    // since ourSession was created on the stack, once we leave this function, it will be deleted
    // and all the other ugens that have been added to it will be deleted too...
}



#pragma mark - SESSION SUBCLASS

// alternatively, you could subclass the session and do all the patch
// creation in the constructor.  if you are not using events (see below)
// this is probably the cleanest way to implement simple things.
struct OurSession : MacOSStereoSession
{
    OurSession()
    {
        // here, we create the ugens and add them on the same line
        auto env = AddUgen(new Envelope({0, 10000, 300}, 10));   // interpolates table over 10 seconds
        auto modulator = AddUgen(new Sine(0, 50));          // sine oscillator at 50hz

        // connect output of envelope into first inlet (amp) of modulator
        *env >> *modulator;
        
        // our carrier, with frequency modulated by another ugen
        auto carrier = AddUgen(new Sine(.5), nullptr, 200 + *modulator);
                
        // hook output of the carrier to both speakers
        MakeConnectionToPatchOutlets(carrier, carrier);
    }
};

void SessionSubclass()
{
    OurSession session;
    session.Run(10); // Starts, sleeps this thread for 10 seconds, then returns
}



#pragma mark - PATCH EXAMPLE

// A Patch is just a Ugen that is a collection of Ugens.
// Here, we create a patch that implements a "simple fm instrument that produces time-varying spectra" from Dodge 5.7 [pg 122]

// Here, we make a Patch subclass and instatiate all of its unit generators inside the patch's constructor.
// But it is possible to add evertying after you make an instance of a Patch, without having to subclass.
// This allows for the possibility of dynamic editing (call it "live coding" if you must)

// define our patch
struct MyPatch : Patch
{
    // patch will have 4 inlets (carr-amp, carr-freq, mod-amp, mod-freq) and one outlet
    MyPatch() : Patch(4, 1)
    {
        // create our ugens
        OnOff *trigger = new OnOff(2,2);  // a gate object, alternating 2 sec ON (output=1), and 2 sec OFF (output=0)
        ADSR *modulatorAmpEnv = new ADSR(.5, 0, 1, 1);
        ADSR *carrierAmpEnv = new ADSR(.1, .05, .5, 1, .5);
        Sine *modulator = new Sine(1, 400, false);
        Sine *carrier = new Sine(.3, 200, false);
        
        // add ugens to patch
        AddUgen(trigger);
        AddUgen(modulatorAmpEnv);
        AddUgen(carrierAmpEnv);
        AddUgen(modulator);
        AddUgen(carrier);
        
        // hook up our connnections to the modulator
        MakeConnection(this, 2, modulatorAmpEnv, 4);        // mod-amp inlet into the amp envelope
        MakeConnection(modulatorAmpEnv, 0, modulator, 0);   // mod amp envelope into amp of the modulator
        MakeConnection(this, 3, modulator, 1);              // freq-modulator inlet into freq of the modulator
        
        // now hook up carrier - note the first 2 use a convenience routine that connects the 1st inlet/outlet of each unit generator
        MakeConnection(this, carrierAmpEnv);        // amp inlet into amp envelope
        MakeConnection(carrierAmpEnv, carrier);     // amp envelope into amp of carrier
        MakeConnection(this, 1, carrier, 1);        // freq-car into frequency of carrier
        MakeConnection(modulator, 0, carrier, 1);   // output of modulator into freq of carrier
                                                    //   (will be automatically added to the other signal connected to this inlet)
        
        // ADSR unit generators support "releasing", which requires a different function to hook up the on/off trigger
        MakeConnectionToUgenOn(trigger, 0, modulatorAmpEnv);
        MakeConnectionToUgenOn(trigger, 0, carrierAmpEnv);
        
        // our outlet
        MakeConnectionToPatchOutlets(carrier);
    }
};

void PatchExample()
{
    // create our session, specifying sr
    MacOSStereoSession *ourSession = new MacOSStereoSession;
    
    // make some control signals (in the real-world these would probably be controleld from a UI)
    Ugen *carAmp = ourSession->AddUgen(new Number(.3));
    Ugen *carFreq = ourSession->AddUgen(new Wavetable({300, 400, 40, 1000, 2, 100, 500, 300}, 1, 1));
    Ugen *modAmp = ourSession->AddUgen(new Number(300 * 7)); // 7 is our "index of modulation"
    Ugen *modFreq = ourSession->AddUgen(new Number(300));

    // instantiate our patch and add it to the session with the control inputs
    MyPatch *p = new MyPatch();
    ourSession->AddUgen(p, carAmp, carFreq, modAmp, modFreq);
    
    // hook up the patch's outputs
    ourSession->MakeConnectionToPatchOutlets(p, p);
    
    // run it for 20 seconds
    ourSession->Run(20);
    
    // cleanup -- deleting session will stop the audio and delete all unit generators
    delete ourSession;
}



#pragma mark - EVENT EXAMPLE
    
// Events are Patches that are scheduled with a specified start time and (optionally) a duration.
// Generally, you define an Event subclass, then instantiate
// an object of that event, passing it to the Session's "AddEvent"
// routine to schedule it.

struct AnEvent : Event
{
    AnEvent(SonoFloat start, SonoFloat cps) : Event(start, 2)
    {
        Ugen *env = AddUgen(new ADSR(1,0,1,1, Random::Generate(.07,.05)));
        Ugen *osc = AddUgen(new Sine(1, cps, false), env);

        // and outlet
        MakeConnectionToPatchOutlets(osc, osc);
    }
};

void LotsOSines()
{
    // create our session, specifying sr
    MacOSStereoSession ourSession;
    
    // allocate 2000 instances of our event in a random start time,
    // events run on separate threads to take
    // advantage of multicore machines)
    for (int i = 0; i < 2000; i++)
    {
        ourSession.AddEvent(new AnEvent((float)i/10, 200 + 50*i));
    }

    // do it!
    ourSession.Run(100);
}



#pragma mark - EVENT EXAMPLE (Risset Bell with scheduled notes)

// risset bell - from Dodge 4.28 (pg 105)

struct RissetBell : Event
{
    RissetBell(SonoFloat start, SonoFloat dur, SonoFloat amp = .5, SonoFloat cps = 500) : Event(start, dur)
    {
        // multiply values for each oscillator (SonoTable is just a std::vector<SonoFloat>)
        SonoTable ampFac = {1, .67, 1, 1.8, 2.67, 1.67, 1.45, 1.33, 1.33, 1, 1.33};
        SonoTable durFac = {1, .9, .65, .55, .325, .35, .25, .2, .15, .1, .075};
        SonoTable freqFac = {.56, .561, .92, .922, 1.19, 1.7, 2, 2.74, 3, 3.76, 4.07};
        
        // 11 partials
        for (int i = 0; i < 11; i++)
        {
            Ugen *env = AddUgen(new Envelope(TableGen::BreakPointExp({0,1,2, 1,400,.5, 0}), dur * durFac[i], amp * ampFac[i]));
            Ugen *osc = AddUgen(new Sine(1, cps * freqFac[i]), env);
            
            // randomize pan
            SonoFloat pan = Random::Generate(1);
            
            // connect to output -- notw we use a '*' operator to multiply the pan value by the osc object
            MakeConnectionToPatchOutlets(*osc * sqrt(pan), *osc * sqrt(1-pan));
        }
    }
};

void Bells()
{
    // create our session (stereo), specifying sr
    MacOSStereoSession ourSession;
    
    // schedule a bunch of notes
    SonoFloat gesture = 4;
    int count = 0;
    
    for (int i = 0; i < 1000; i++)
    {
        if (count++ > 10)
        {
            gesture = .5+Random::Generate(10);
            count = 0;
        }
        
        auto note = new RissetBell(Random::Generate(100), .1+gesture*Random::Generate(4), .01+Random::Generate(.05), 50+Random::Generate(10000));
        ourSession.AddEvent(note);
    }
    
    ourSession.Run(1010);
}


#pragma mark - EVENT EXAMPLE (Granular with each grain a generated note in real-time)

struct Sample : Event
{
    Sample(SonoFloat start, SonoFloat dur, SonoFloat amp = .5, SonoFloat transp = 1) : Event(start, dur)
    {
        // create a static wavetable that we will use to cache the soundfile data
        // so we don't need to reload it for every instance
        if (!mTable) {
            mTable = new Wavetable("/Users/matt/Music/SamplesSoundFX/kindling.aiff");
        }
        
        // create a new ugen by copying
        Wavetable *wave = new Wavetable(*mTable);
        wave->SetAmp(1);
        wave->SetCps(transp * mTable->Cps());
        AddUgen(wave);

        MakeConnection(wave, 0, this, 0);
        MakeConnection(wave, 1, this, 1);
    }
    
    // a static variable to avoid loading
    static Wavetable *mTable = nullptr;
};

Wavetable* Sample::mTable = nullptr;

void Granular()
{
    // create our session (stereo), specifying sr
    MacOSStereoSession ourSession;
    ourSession.Start();
    
    // generate a bunch of notes, continually (until you kill the process)
    SonoFloat gesture = 4;
    SonoFloat time = .05;

    int count = 0;
    
    while (true)
    {
        if (count++ > 35)
        {
            gesture = 1+pow(Random::Generate(200), .5);
            time = .05;
            count = 0;
        }
        
        double cps = .75+pow(Random::Generate(.1), gesture/16);
        ourSession.AddEvent(new Sample(0, .1, Random::Generate(.01, 10), cps));
        cps *= Random::Generate(cps, 4*cps);
        ourSession.AddEvent(new Sample(0, .1, Random::Generate(.01, 10), cps));

        Sleep::For(Random::Generate(time));
        time *= 1.2;
    }
}

#pragma mark - PWM + LP

struct PWMOsc : Ugen
{
    PWMOsc()
    {
        CreateInlet(&mPeriod);
        CreateOutlet();
    }
    
protected:
    void ProcessSample()
    {
        mFirstOutlet->value = mValue;
        SonoFloat limit = (mValue > 0 ? mPeriod * .5 : mPeriod * .5); // square = .5/.5, change for waveforms
        
        if (++mIndex > limit)
        {
            mIndex = 0;
            mValue = -mValue;
        }
    }
    
    SonoFloat mIndex = 0;
    SonoFloat mPeriod = 1000;
    SonoFloat mValue = .2;
};

void UgenExample()
{
    // create our session (stereo)
    MacOSStereoSession ourSession;
    
    // freq
    auto rnd = ourSession.AddUgen(new White(250, 1), nullptr, ourSession.Add(.4, new White(.4, 5)));
    auto freq = ourSession.AddUgen(new Port(.0002), ourSession.Add(ourSession.Mult(rnd, rnd), 20));
    
    // osc
    auto pwm = ourSession.AddUgen(new PWMOsc(), freq);
    
    // filter
    auto cf = ourSession.AddUgen(new White(3000, .5));
    auto portCF = ourSession.AddUgen(new Port(.001), ourSession.Add(cf, 3020));
    auto reson = ourSession.AddUgen(new White(.1, .3));
    auto portReson = ourSession.AddUgen(new Port(.0001), ourSession.Add(reson, .899));
    auto lp = ourSession.AddUgen(new Lowpass(10000, .9), pwm, portCF, portReson);

    // hook it to the outputs
    ourSession.MakeConnectionToPatchOutlets(lp, lp);
    
    // run it
    ourSession.Run(200);
}



int main (int argc, char * const argv[])
{
    bool running = true;
    while (running)
    {
        std::cout << "============================\n"
        << "SonoDSP Examples, enter the option you wish to execute:\n"
        << "============================\n"
        << "1 = HELLO WORLD\n"
        << "2 = HELLO WORLD (MORE DETAILS)\n"
        << "3 = SESSION SUBCLASS\n"
        << "4 = PATCH EXAMPLE\n"
        << "5 = EVENT EXAMPLE 0\n"
        << "6 = EVENT EXAMPLE 1\n"
        << "7 = EVENT EXAMPLE 2\n"
        << "8 = UGEN EXAMPLE\n"
        << "q = quit\n"
        << "============================\n";
        
        char inKey;
        std::cin >> inKey;
        switch (inKey)
        {
            case '1':
                HelloWorld();
                break;
                
            case '2':
                HelloWorldMore();
                break;
                
            case '3':
                SessionSubclass();
                break;
                
            case '4':
                PatchExample();
                break;
                
            case '5':
                LotsOSines();
                break;
                
            case '6':
                Bells();
                break;
                
            case '7':
                Granular();
                break;
                
            case '8':
                UgenExample();
                break;
                
            case 'q':
                std::cout << "bye!\n";
                running = false;
                break;

            default:
                std::cout << "invalid option\n";
                break;
        }
    }
}
